## ARTF O'Reilly Mobile

The Automation Robot Test Framework for mobile version of [O'Reilly application](https://en.softonic.com/download/oreilly/android/post-download/v/5.9.3) that is popular among Developers.


#### Prepare the environment
* create virtual environment;
* setup requirements;
* configurate the Android Studio;
* install Appium and UI Automator Viewer.


#### Test Suites
The tests are located in the [Tests](Tests) and contains positive cases:
* Signing screen [test_oreilly_signin_screen.robot](Tests/test_oreilly_signin_screen.robot);
* Password screen [test_password_screen.robot](Tests/test_password_screen.robot) and successful signing;
* Cancel signing [test_cancel_signin.robot](Tests/test_cancel_signin.robot);
* Forgot password [test_forgot_password.robot](Tests/test_forgot_password.robot) before Signin.
</br></br>
Check the Test Cases work with the command:

```commandline
robot Tests/
```


#### Report logs and screens

![img_1.png](data/img_1.png)

![img_2.png](data/img_2.png)

![img.png](data/img.png)

