*** Settings ***
Documentation   Cancel Signin to OReilly on initial Signin screen

Library         AppiumLibrary
Resource        ../Resources/resources.resource


*** Test Cases ***
Cancel Signin
    Open Oreilly Application
    Signin with Existed Email
    Click Element                       ${CANCEL_BUTTON}
    Check Elements on SignIn screen
    Close Application
