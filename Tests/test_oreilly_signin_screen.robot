*** Settings ***
Documentation    Check elements on Signin screen O'Reilly before Signin

Library         AppiumLibrary
Resource        ../Resources/resources.resource


*** Test Cases ***
Elements on SignIn screen are present
    Open Oreilly Application
    Check Elements on SignIn screen
    Close Application
