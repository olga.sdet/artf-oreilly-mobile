*** Settings ***
Documentation   Forgot Password during Signin to OReilly on Password screen

Library         AppiumLibrary
Resource        ../Resources/resources.resource


*** Test Cases ***
Forgot Password Signin
    Open Oreilly Application
    Signin with Existed Email
    Click Element                       ${FORGOT_PASSWORD_SECTION}
    Check Elements on Forgot Password Popup
    Close Application
