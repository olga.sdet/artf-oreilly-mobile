*** Settings ***
Documentation   Check elements on Password screen O'Reilly before Signin and after successful Signin

Library         AppiumLibrary
Resource        ../Resources/resources.resource


*** Test Cases ***
Elements on Password screen are present
    Open Oreilly Application
    Signin with Existed Email
    Check Elements on Password screen
    Close Application

Signin with existed Password
    Open Oreilly Application
    Signin with Existed Email
    Input Text                          ${PASSWORD_INPUT_FIELD}  ${PASSWORD}
    Click Element                       ${SIGNIN_BUTTON}
    Check Elements on Landing Screen
    Close Application
